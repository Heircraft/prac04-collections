package question;

import java.util.AbstractMap;
import java.util.Map;

public abstract class Bag<K, V> extends AbstractMap<K, V> {
	//final Map<K, Integer> map;
	
	public abstract void add(K e,int n);
	
	public abstract void remove(K e,int n);
	
	public abstract int quantity(K e);
	
	public abstract int toSet();
	
	public abstract int size();
	
	public abstract int iterator();
	
}
