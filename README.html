<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
  <title></title>
  <style type="text/css">code{white-space: pre;}</style>
  <link rel="stylesheet" href="imgs/kevinburke.markdown.css" type="text/css" />
</head>
<body>
<h1 id="cab302-software-development">CAB302 Software Development</h1>
<h1 id="practical-4-collections">Practical 4: Collections</h1>
<p>This week's practical exercise will give you experience at designing and implementing your own extension to the standard Java Collections Framework.</p>
<hr />
<h2 id="background">Background</h2>
<p>Bags, also known as <em>multisets</em>, are a data structure consisting of an <em>unordered</em> collection of elements in which <em>duplicate elements</em> are allowed. This means that not only can we ask whether an element occurs in the collection, as we can with sets, but we can also ask how many times it does so.</p>
<p>According to one of Oracle's Frequently Asked Questions pages, the standard Java Collections Framework does not include bags as a type because they are not needed very often. (The fact that Oracle is <em>frequently</em> asked about bags makes one wonder if this is true!) Fortunately, we can easily implement our own bag type if we need them. Indeed, there are many different Java bag implementations on the web and the Java Collection API specification even mentions the need or them.</p>
<p>Our overall goal is therefore to add a bag type to the Java collections framework. A JUnit test class file <code>BagTest.java</code> has been provided to help you get started (although you will need to modify these tests to keep up with your implementation). An exception class file <code>BagException.class</code> has also been provided for use in your solution.</p>
<hr />
<h2 id="exercise-1-designing-the-type">Exercise 1: Designing the type</h2>
<p>Before beginning the implementation you need to decide exactly what operations your bag class will provide and how it links into the existing Collections Framework.</p>
<p>Consider the interface classes at the top level of Java's Collections Framework, i.e., the <code>Collection</code>, <code>Set</code>, <code>List</code>, <code>Map</code> and <code>Queue</code> types (plus the <code>Array</code> utilities).<br />Given the characteristics of a bag described above, underneath which class in this hieararchy would a <code>Bag</code> class belong? Keep in mind the principles of subtyping -- a subtype must be usable wherever its supertype is expected. In particular, is a multiset (i.e., a bag) usable where a set is expected?</p>
<p>You also need to consider which operations are essential to allow the new type to be used successfully. We suggest that the following operations are especially useful for bags:</p>
<ul>
<li><code>add(e, n)</code> -- adds <em>n</em> copies of element <em>e</em> to the bag,</li>
<li><code>remove(e, n)</code> -- removes <em>n</em> copies of element <em>e</em> from the bag,</li>
<li><code>quantity(e)</code> -- returns the number of copies of element <em>e</em> in the bag (which may be zero),</li>
<li><code>toSet()</code> -- returns a set comprising all of the distinct elements in the bag (i.e. with no duplicates),</li>
<li><code>size()</code> -- returns the number of elements in the bag, counting duplicates, and</li>
<li><code>iterator()</code> -- returns an iterator object for bags (which will support 'for each' style <strong>for</strong> loops involving bags).</li>
</ul>
<p>Study the supplied unit tests to see exactly how these methods are used under normal circumstances. (We have not supplied an API document with this exercise because this would reveal aspects of the type hierarchy that we want you to design for yourself.)</p>
<p><strong>NB:</strong> The supplied set of unit tests is not complete, because tests for boundary cases and exceptional situations depend on design decisions that you have to make. For instance, what should the <code>remove</code> method do if asked to remove more copies of a particular element than are in the bag? What should the add method do if asked to <code>add</code> a negative number of items?</p>
<hr />
<h2 id="exercise-2-specifying-the-types-methods">Exercise 2: Specifying the type's methods</h2>
<p>As we have seen, a common idiom in Java's Collections Framework is to define an interface or abstract class to specify the methods available for a particular type, and to then provide one or more concrete classes that implement these methods, each using a different data structure for the underlying representation. For this exercise we will do the same, starting with an abstract <code>Bag</code> class which specifies the operations available for bags.</p>
<p>Recall from the lecture demonstration that an implementation of an interface or abstract class is obliged to implement <em>all</em> of the methods specified in the supertype, of which there may be many. To save effort, Java's Collections Framework provides abstract supertype classes that have default dummy implementations for most non-essential methods. These include <code>AbstractCollection</code>, <code>AbstractSet</code>, <code>AbstractList</code>, <code>AbstractMap</code> and <code>AbstractQueue</code>. Your <code>Bag</code> class should extend one of these classes.</p>
<p>Having chosen to build your <code>Bag</code> type under one of these existing classes, you will construct a type hierarchy as shown in the <em>Type Hierarchy</em> image.</p>
<div class="figure">
<img src="imgs/hierarchy.png" title="Type Hierarchy" alt="Type Hierarchy" /><p class="caption">Type Hierarchy</p>
</div>
<p>The goal of this exercise is to define the abstract class <code>Bag</code> which defines the methods that the bag type must provide, but doesn't implement them. Create a new class called <code>Bag</code> in Eclipse, which extends your chosen superclass, and populate it with specifications for all of the operations peculiar to bags. Note that you don't have to provide specifications for any methods already provided by the superclass. (However, depending on which superclass you use for your <code>Bag</code> class, you may be obliged to implement methods other than those listed above.) Eclipse's <em>Hierarchy</em> view <img src="imgs/hierarchyicon.png" alt="Type Hierarchy" /> is useful for viewing the design of your type hierarchy in the context of the standard Collections Framework types.</p>
<hr />
<h2 id="exercise-3-implementing-the-type">Exercise 3: Implementing the type</h2>
<p>As the figure above shows, given an abstract specification of a class, we can usually implement it in several different ways. In this exercise you are required to extend your <code>Bag</code> specification with a concrete class that implements each of the bag operations, using a particular data structure to represent bags, chosen from the standard Collections Framework types. For instance, an implementation based on the standard <code>LinkedList</code> class would be called <code>LinkedBag</code>, one based on the <code>HashMap</code> class would be called <code>HashBag</code>, and so on.</p>
<p><strong>Hint:</strong> Most of the methods listed above are straightforward to understand and implement. The <code>iterator()</code> method is less obvious, however. In general an iterator method must return an object with three operations, <code>hasNext()</code>, <code>next()</code> and <code>remove()</code>, that allows us to traverse the elements of a collection. (See the JDK API entry for the <code>Iterator</code> class for more detail.) Although you could choose to do this explicitly, by defining a nested class with these three methods, a much easier option for implementing the <code>iterator()</code> method is to simply put your bag's contents in an object of one of the standard data types (ensuring that it is one that preserves duplicates) and then return that object's iterator.</p>
<p>For the purposes of this practical you should attempt to make use of the provided <code>BagException</code> class to throw appropriate exceptions in your implementation. If you are having difficulty with your <code>Bag</code> implementation you should concentrate on a simple implementation without exceptions first and then add exceptions once you have it working.</p>
<hr />
<h2 id="exercise-4-testing-the-type">Exercise 4: Testing the type</h2>
<p>While developing your new type you should test its functionality with the supplied <code>BagTest</code> class to measure your progress in implementing the operations. However, you may need to modify some of the tests to reflect design decisions you have made. In particular, if your methods may throw exceptions you will need to add <strong><code>throws</code></strong> declarations to the corresponding unit tests.</p>
<p>e.g. <code>public void singleElementType()</code> <strong><code>throws BagException</code></strong> <code>{</code></p>
<p>Furthermore, you should add new unit tests to reflect design decisions you have made. In particular, the supplied tests don't check any unusual cases, such as being asked to add a negative number of elements, or remove more elements than the bag contains, so you should add tests that reflect how you chose to resolve these issues.</p>
<hr />
<h2 id="exercise-5-documenting-the-type">Exercise 5: Documenting the type</h2>
<p>Having implemented a new data type you now need to document it so that other programmers know how to use it. Use Javadoc comments to describe your new type and its methods and then generate an API specification for the new type. Note that Javadoc comments you put on method specifications in the <code>Bag</code> class will be automatically copied to their overriding implementations in the concrete class when the API document is generated, so you don't need to copy them into both source code files.</p>
<hr />
<h2 id="hint">Hint</h2>
<p>A HashMap is a suitable collection to use as the basis of this exercise.</p>
</body>
</html>
